var https = require('https');
var qs = require('qs');
var Promise = require("bluebird");
var mergeWith = require("lodash.mergewith");
var Signer = require("./lib/SignatureMODO1");

/**
 * @typedef {Object} ApiLoc
 * @property {string} api_url - Api hostname
 * @property {string} base - basepath
 */

/**
 * @typedef {Object} ApiCred
 * @property {string} key - api key
 * @property {string} secret - api secret
 */

/**
 * @typedef {Object} ModoRequest
 * @property {Object} auth
 * @property {ApiCred} auth.credentials
 * @property {ModoSig} auth.signature
 * @property {string} body
 * @property {Object} endpoint
 * @property {string} endpoint.uri
 * @property {ApiLoc} endpoint.location
 */

/**
 * @namespace modo
 * @exports modoapi/modo
 */
module.exports = {
	call: call,
	buildRequest: buildRequest,
	sendRequest: sendRequest,
	signRequest: Signer.signRequest
};

/**
 * Build, sign, and send a modo request
 * @memberOf modo
 * @param {string} endpoint - modo endpoint
 * @param {Object|undefined} params - request parameters
 * @param {ApiLoc} location
 * @param {ApiCred} credentials
 * @return {Promise}
 */
var call = function (endpoint, params, location, credentials) {
	var request = buildRequest(endpoint, params, location, credentials);
	Signer.signRequest(request);
	return sendRequest(request);
};

/**
 * Build a modo request object
 * @memberOf modo
 * @param {string} endpoint - modo endpoint
 * @param {Object|undefined} params - request parameters
 * @param {ApiLoc} location
 * @param {ApiCred} credentials
 * @return {ModoRequest}
 */
var buildRequest = function (endpoint, params, location, credentials) {
	"use strict";
	/**@type {ModoRequest}*/
	var request = {
		auth: {
			credentials: credentials,
			signature: undefined
		},
		body: (params) ? qs.stringify(params) : "",
		endpoint: {
			location: location,
			uri: location.base + endpoint
		}
	};
};

/**
 * Sign and send a request to modo core
 * @memberOf modo
 * @param {ModoRequest} request - Request to send
 * @param {Object.<string,string>} [additionalHeaders] - Map of additional headers to include
 * @return {Promise}
 */
var sendRequest  = function (request, additionalHeaders) {
	"use strict";
	var httpsRequest = {
		hostname: request.endpoint.location.api_url,
		path: request.endpoint.uri,
		port: 443,
		method: "POST",
		headers: {
			"X-Modo-Date": "",
			"Authorization": "",
			"Content-Type": "application/x-www-form-urlencoded"
		}
	};
	if (!request.auth.signature){
		throw new Error("Request is not signed")
	}
	httpsRequest.headers["X-Modo-Date"] = request.auth.signature.timestamp;
	httpsRequest.headers["Authorization"] = request.auth.signature.authHeader;
	if (additionalHeaders!==undefined){
		mergeWith(httpsRequest.headers, additionalHeaders);
	}

	return new Promise(function (resolve) {
		"use strict";
		var responseBody = "";
		var req = https.request(httpsRequest, function (res) {
			"use strict";
			res.setEncoding('utf8');
			res.on('data', function (chunk) {
				responseBody += chunk;
			});
			res.on('end', function () {
				var content = safeJSONParse(responseBody);
				if (content.status_code == 401) {
					resolve({response_data: {access_token: 0, original: content}});
				} else if (content.status_code >= 300) {
					resolve(responseBody);
				} else {
					resolve(content);
				}
			})
		});
		req.on('end', function (err) {
			"use strict";
			resolve(err);
		});
		req.write(request.body);
		req.end()
	});
};

/**
 * @private
 */
function safeJSONParse(input) {
	try {
		return JSON.parse(input)
	} catch (e) {
		return {response: input};
	}
}

