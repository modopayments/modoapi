#modoapi
*A convenience library for sending modo api request*

##Install 
```bash
npm install modoapi
```

##Usage
First, you'll need to require the module and setup your api credentials and api location

```js
var modo = require("modoapi");

var apiCreds = {
	key: "YOUR CONSUMER KEY",
	secret: "YOUR CONSUMER SECRET KEY"
};

var apiLoc = {
	api_url: "MODO API URL",
	base: "MODO API BASEPATH"
};
```

There are two ways to send a request

**Easy way**
```js
/* Lookup a person by phone */
modo.call("people/lookup_phone", {phone: "5551230000"}, apiLoc, apiCreds).then(function(response){
	// Request was accepted and a response was received
	// Does NOT mean that it did what you expected, always check the response data
	console.log(JSON.stringify(response));
}, function(err){
	// Something went horribly wrong with the request, auth failed, etc ... 
	console.error(err);
});
```

**Ever so slightly less easy way**
```js
/* Get a list of merchants */
// Build the request 
var merchantRequest = modo.buildRequest("merchant/list", null, apiLoc, apiCreds);
// Sign the request
modo.signRequest(merchantRequest);
// Send the request
modo.sendRequest(merchantRequest).then(function(response){
	// Request was accepted and a response was received
	// Does NOT mean that it did what you expected, always check the response data
	console.log(JSON.stringify(response));
}, function(err){
	// Something went horribly wrong with the request, auth failed, etc ... 
	console.error(err);
});
```
