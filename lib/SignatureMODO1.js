var crypto = require('crypto');
var SIGN_VERSION = 'MODO1';

/**
 * @typedef {Object} ModoSig
 * @property {int} timestamp
 * @property {string} authHeader
 */

/**
 * Sign and send a request to modo core
 * @memberOf modo
 * @param {ModoRequest} request
 * @return {ModoRequest}
 */
module.exports.signRequest = function (request) {
	/**@type {ModoSig}*/
	var modoSig = {
		timestamp: Date.now(),
		authHeader: ""
	};
	var bodyHash = crypto.createHash("sha256").update(request.body).digest('hex');
	var signingKey = crypto.createHmac("sha256", SIGN_VERSION + request.auth.credentials.secret).update(modoSig.timestamp.toString()).digest();
	var stringToSign = modoSig.timestamp + "&" + request.endpoint.uri + "&" + bodyHash;
	var signature = crypto.createHmac("sha256", signingKey).update(stringToSign).digest('hex');
	modoSig.authHeader = SIGN_VERSION + ' key=' + request.auth.credentials.key + ', sig=' + signature;
	request.auth.signature = modoSig;
	return request;
};